Welcome to the Musium project, an online jukebox with user accounts and CRUD for admins.

I made this project at the end of the 6+ months training I followed at Technofutur TIC in Belgium.
I used TSQL, ADO.NET, ASP.NET, JWT and Angular for this project.

Unfortunately, a live version is not available as hosting a C# .NET6 API is quite expensive. However, I recorded a live demo of the project in its current state: https://youtu.be/sl71nO8DCQg

Feel free to look at the code and ask any questions.
https://www.linkedin.com/in/nicolas-hbt/
